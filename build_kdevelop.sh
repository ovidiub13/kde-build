#!/bin/bash

: "${KDESRC:=$HOME/KDE/source}"
: "${KDEBUILD:=$HOME/KDE/build}"
: "${KDEINSTALL:=$HOME/KDE/install}"

mkdir -p $KDESRC $KDEBUILD $KDEINSTALL

if [ -z ${1+x} ]; then
    ARGS="libkomparediff2 grantlee kdevplatform kdevelop-pg-qt kdevelop"
else
    ARGS=$@
fi

docker run \
    --rm \
    -ti \
    -u `id -u`:`id -g` \
    \
    -v $KDESRC:/source \
    -v $KDEBUILD:/build \
    -v $KDEINSTALL:/install \
    \
    kde-build \
    $ARGS
